package com.javaleague.oo.r1;

public class Board {
	
	private String[][] data;
	
	public String collectData() {
		StringBuffer buf = new StringBuffer();
		for(int i=0; i<10; i++) {
			for (int j=0; j<10; j++) {
				buf.append(data[i][j]).append("\t");
			}
			buf.append("\n");
		}
		return buf.toString();
	}
	
}
