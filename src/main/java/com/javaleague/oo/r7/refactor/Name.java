package com.javaleague.oo.r7.refactor;

public class Name {
	
	Surname surname;
	
	GivenName givenName;
	
	class Surname {
		String first;
	}
	
	class GivenName {
		String middle;
		
		String last;
	}
}
