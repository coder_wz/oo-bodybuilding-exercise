package com.javaleague.oo.r3;

import java.util.Calendar;
import java.util.Date;

public class Example3 {
	
	public Date createDate(int year, int month, int date) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DATE, date);
		return calendar.getTime();
	}
	
}
