package com.javaleague.oo.exercise.pay.refactor;

public class Customer {
    private Name name;
    private Wallet myWallet;
    public Name getName() {
		return name;
	}
    public Customer(Name name, Wallet myWallet) {
		super();
		this.name = name;
		this.myWallet = myWallet;
	}
	public Wallet getWallet(){
        return myWallet;
    }
    
    public void pay(Money debit) {
    	float total = myWallet.getTotalMoney();
    	if (total < debit.getValue()) {
    		throw new RuntimeException("哟呵， 买多了。钱不够");
    	}
    	myWallet.takeMoney(debit.getValue());
    }
    
}
