package com.javaleague.oo.exercise.pay.refactor;


public class Money extends ObjectWrapper {

	public Money(Float value) {
		super(value);
	}
	
	@Override
	public Float getValue() {
		return (Float)super.getValue();
	}
	
	public Money add(float amount) {
		return new Money(getValue() + amount);
	}
}
