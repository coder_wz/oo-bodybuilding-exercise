package com.javaleague.oo.exercise.pay.refactor;

public class StringWrapper extends ObjectWrapper {

	public StringWrapper(String value) {
		super(value);
	}
	
	@Override
	public String getValue() {
		return (String)super.getValue();
	}
}
