package com.javaleague.oo.exercise.pay.refactor;

public class Name {

	private String firstName;
	private String lastName;

	public static class FirstName extends StringWrapper {

		public FirstName(String value) {
			super(value);
		}
		
	}
	
	public static class LastName extends StringWrapper {

		public LastName(String value) {
			super(value);
		}
		
	}

	public Name(FirstName firstName, LastName lastName) {
		super();
		this.firstName = firstName.getValue();
		this.lastName = lastName.getValue();
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

}
