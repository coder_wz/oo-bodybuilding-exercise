package com.javaleague.oo.exercise.pay;

public class Customer {
    private String firstName;
    private String lastName;
    private Wallet myWallet;
    
    
    public Customer(String firstName, String lastName, Wallet myWallet) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.myWallet = myWallet;
	}
	public String getFirstName(){
        return firstName;
    }
    public String getLastName(){
        return lastName;
    }
    public Wallet getWallet(){
        return myWallet;
    }
}
