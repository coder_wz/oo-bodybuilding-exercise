package com.javaleague.oo.exercise.moviestore.refactor;

import java.util.List;

import com.javaleague.oo.exercise.moviestore.refactor.rental.Rental;
import com.javaleague.oo.exercise.moviestore.refactor.rental.Rentals;

public class DefaultPrintStragety implements PrintStrategy {
	
	@Override
	public String print(Customer customer) {
		Rentals rentals = customer.getRentals();
		StringBuilder builder = new StringBuilder();
		List<Rental> value = rentals.getValue();
		renderPageHeader(customer, builder);
		renderPageContent(builder, value);
		renderPageFooter(rentals, builder);
		return  builder.toString();
	}

	private void renderPageContent(StringBuilder builder, List<Rental> value) {
		for (Rental rental : value) {
			builder.append("\t").append(rental.getMovie().getTitle()).append("\t").append(String.valueOf(rental.getAmount()));
			builder.append("\n");
		}
	}

	private void renderPageFooter(Rentals rentals, StringBuilder builder) {
		builder.append("Amount owed is ").append(String.valueOf(rentals.getTotalAmount())).append("\n");
		builder.append("You earned ").append(String.valueOf(rentals.getFrequentRenterPoints())).append(" frequent renter points");
	}

	private void renderPageHeader(Customer customer, StringBuilder builder) {
		builder.append("Rental Record for ").append(customer.getName()).append("\n");
	}
	
}
