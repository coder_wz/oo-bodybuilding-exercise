package com.javaleague.oo.exercise.moviestore.refactor.movie;

public class PriceCode {
	public static final PriceCode REGULAR = new PriceCode(0);
	public static final PriceCode NEW_RELEASE = new PriceCode(1);
	public static final PriceCode CHILDREN = new PriceCode(2);
	private int value;

	private PriceCode(int status) {
		this.value = status;
	}

	public int getValue() {
		return value;
	}
}
