package com.javaleague.oo.exercise.moviestore.refactor.rental;

import com.javaleague.oo.exercise.moviestore.refactor.movie.ChildrenMovie;
import com.javaleague.oo.exercise.moviestore.refactor.movie.Movie;
import com.javaleague.oo.exercise.moviestore.refactor.movie.NewReleaseMovie;
import com.javaleague.oo.exercise.moviestore.refactor.movie.RegularMovie;

public class Rental {
    private Movie movie;
    private int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public int getDaysRented() {
        return daysRented;
    }

    public Movie getMovie() {
        return movie;
    }
    
    public double getAmount() {
    	if (movie instanceof RegularMovie) {
    		return daysRented <= 2 ? 2 : 2 + (daysRented - 2) * 1.5;
    	}
    	if (movie instanceof NewReleaseMovie) {
    		return daysRented * 3;
    	}
    	if (movie instanceof ChildrenMovie) {
    		return daysRented <= 3 ? 1.5 : 1.5 + (daysRented - 3) * 1.5;
    	}
    	return otherMovieAmount();
    }
    
    protected double otherMovieAmount() {
    	throw new UnsupportedOperationException();
    }
    
    public int getFrequentRenterPoints() {
    	if ((movie instanceof NewReleaseMovie)
    			&& (daysRented > 1)) {
    		return 2;
    	}
    	return 1;
    }
}
