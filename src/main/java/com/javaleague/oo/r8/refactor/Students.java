package com.javaleague.oo.r8.refactor;

import java.util.ArrayList;
import java.util.List;

public class Students {
	
	private List<Student> studentList = new ArrayList<Student>();
	
	public void register(Student student) {
		studentList.add(student);
	}
	
}
