package com.javaleague.oo.r2.refactor;

public class Example {

	private int status;

	private static final int DONE = 9;

	public void endMe() {
		if (status == DONE) {
			doSomething();
			return;
		} 
		
		doElse();
	}

	private void doElse() {
		
	}

	private void doSomething() {

	}
}
